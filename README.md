# NetworkManager 


Android Library for consume REST services. A Retrofit wrapper.

[ ![Download](null/packages/jdsilval/android/co.jsilval.networkmanager/images/download.svg?version=1.0.9) ](https://bintray.com/jdsilval/android/co.jsilval.networkmanager/1.0.9/link)

## Support
- JSON and XML content type
- POST
- GET
- PUT
- DELETE
- PATCH

## Using


### Implementation
`implementation 'co.jsilval.networkmanager:network-manager:latestVersion'`

replace `latestVersion` with the last version, check [here](https://bintray.com/jdsilval/android/co.jsilval.networkmanager)


#### Define a base url
`NetworkManager.getInstance().setBaseURL("http://localhost:3000/");`

#### * POST 

Consuming the service: `/api/Users`   
Parameter Type: `body`   
Value:

```json
{
  "ID": 0,
  "UserName": "string",
  "Password": "string"
}
```

##### Set the client
```java
UserPost user = new UserPost();
user.setID(1);
user.setPassword("12345");
user.setUserName("John");
 
 final ServerRequest serverRequest = ServerRequest.create("/api/Users")
                .header(Header.CONTENT_TYPE, "application/json")
                .body(user);

 NetworkClient client = new NetworkClient(serverRequest, HttpMethod.POST);
 client.execute(new NetworkResponse<UserPost>() {
 }, this);
```

#### * GET
Consuming the service: `/api/Users/{id}`   
Parameter Type: `path`   
Value: `id`

##### Set the client

```java
final ServerRequest serverRequest = ServerRequest.create("/api/Users/3")
			.header(Header.CONTENT_TYPE, "application/json")
            .header(Header.ACCEPT_LANGUAGE, "es-CO");

NetworkClient client = new NetworkClient(serverRequest, HttpMethod.GET);
client.execute(new NetworkResponse<UserResponse>() {
}, this);
```
#### * PUT
Consuming the service: `/api/Users/{id}`   
Parameter Type: `path`, `body`   
Value: `id`
```json
{
  "ID": 0,
  "UserName": "string",
  "Password": "string"
}
```
##### Set the client

```java
UserPut user = new UserPut();
user.setID(1);
user.setPassword("12345");
user.setUserName("John");

final ServerRequest serverRequest = ServerRequest.create("/api/Users/2")
			.header(Header.CONTENT_TYPE, "application/json")
			.body(user);

NetworkClient client = new NetworkClient(serverRequest, HttpMethod.PUT);
client.execute(new NetworkResponse<UserPut>() {
}, this);
```
#### * DELETE
Consuming the service: `/api/Users/{id}`   
Parameter Type: `path`   
Value: `id`

```java
final ServerRequest serverRequest = ServerRequest.create("/api/Users/2")
			.header(Header.CONTENT_TYPE, "application/json");

NetworkClient client = new NetworkClient(serverRequest, HttpMethod.DELETE);
client.execute(new NetworkResponse<Object>() {
}, this);
```

## Handle Asynchronous request
```java
/*
 * Success response from server
 * */
@Override
public void responseSuccess(@Nullable UserPost response) {
}

/*
 * Server error
 * */
@Override
public void responseError(Response<?> response) {
}

/*
 * Other kind of error, for example network
 * */
@Override
public void otherError(Throwable t) {
}

/*
 * Conversion error
 * */
@Override
public void onFailure(Throwable t) {
}
```

### LiveData response

```java
final LiveData<ApiResponse<List<UserResponse>>> liveData = client.getLiveData(
    new NetworkResponse<List<UserResponse>>() {
});

liveData.observe((AppCompatActivity) context, apiResponse -> {
    if (apiResponse instanceof ApiSuccessResponse) {
        final List<UserResponse> list = 
            ((ApiSuccessResponse<List<UserResponse>>) apiResponse).getBody();
        Log.d("TAG", "onChanged: " + list.size());
    }
});
```

### Synchronous request

```java
final String execute = client.execute(new NetworkResponse<String>(){});
```
# Developed By

* Jose Silva (jose.silva@linuxmail.org)