package co.jsilval.apimanager.core.calladapter

import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse

interface ResponseCallback<T> {
    /* Called for [200, 300] responses, except 204. */
    fun responseSuccess(response: ApiSuccessResponse<T>)

    /* Called for 204 response. */
    fun responseEmpty(response: ApiEmptyResponse)

    /* Called for all non-2xx responses. */
    fun responseError(response: ApiErrorResponse)

    /* Called for network or unexpected errors. */
    fun otherError(t: Throwable)

    /* Called when there was a conversion error. */
    fun conversionError(t: Throwable)
}