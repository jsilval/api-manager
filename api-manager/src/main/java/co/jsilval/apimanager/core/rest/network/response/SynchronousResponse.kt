package co.jsilval.apimanager.core.rest.network.response


class SynchronousResponse<T>(val errorResponse: String?, val responseCode: Int, val response: T?)