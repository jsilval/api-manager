package co.jsilval.apimanager.core.calladapter

import co.jsilval.apimanager.core.rest.network.response.SynchronousResponse

interface ResponseCall<T> {
    fun cancel()
    fun enqueue(callback: ResponseCallback<T>)
    fun execute(): SynchronousResponse<T>?
}