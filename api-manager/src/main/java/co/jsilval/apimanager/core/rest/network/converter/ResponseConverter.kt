package co.jsilval.apimanager.core.rest.network.converter

import com.google.gson.reflect.TypeToken
import com.google.gson.Gson
import org.simpleframework.xml.Serializer
import org.simpleframework.xml.core.Persister
import java.lang.Exception
import java.lang.UnsupportedOperationException
import java.util.*

object ResponseConverter {
    @JvmStatic
    @Suppress("UNCHECKED_CAST")
    fun <T> convert(typeOfT: TypeToken<T>, input: String, contentType: String): T? {
        try {
            if (typeOfT.type === String::class.java) {
                return input as T
            }
            return if (contentType.lowercase(Locale.getDefault()).contains("json")) {
                Gson().fromJson(input, typeOfT.type)
            } else if (contentType.lowercase(Locale.getDefault()).contains("xml")) {
                val serializer: Serializer = Persister()
                serializer.read(typeOfT.rawType, input) as T
            } else {
                throw UnsupportedOperationException(
                    "ContentType: $contentType not supported")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }
}