package co.jsilval.apimanager.core.rest

import co.jsilval.apimanager.core.rest.field.Field
import co.jsilval.apimanager.core.rest.headers.CustomHeader
import co.jsilval.apimanager.core.rest.headers.Header
import co.jsilval.apimanager.core.rest.network.TypeRequest

class ServerRequest private constructor(
    val typeRequest: TypeRequest? = null,
    val endPoint: String = "",
    val authToken: String? = null,
) {
    private val headers = HashMap<String, CustomHeader>()
    private val fields = HashMap<String, Field>()
    var body: Any? = null
    val pathData = HashMap<String, String>()
    var contentType = ""
        private set

    fun path(key: String, value: String): ServerRequest {
        pathData[key] = value
        return this
    }

    fun header(header: Header, value: String): ServerRequest {
        headers[header.key] = CustomHeader.create(header.key, value)
        if (header == Header.CONTENT_TYPE) {
            contentType = value
        }
        return this
    }

    fun header(key: String, value: String): ServerRequest {
        headers[key] = CustomHeader.create(key, value)
        if (key.trim { it <= ' ' }
                .equals(Header.CONTENT_TYPE.key, ignoreCase = true)) {
            contentType = value
        }
        return this
    }

    fun body(body: Any): ServerRequest {
        this.body = body
        return this
    }

    fun field(name: String, value: String): ServerRequest {
        val field = Field.create(name, value)
        fields[name] = field
        return this
    }

    fun fields(vararg fields: Field): ServerRequest {
        for (field in fields) {
            this.fields[field.name] = field
        }
        return this
    }

    fun fieldList(): List<Field> {
        return ArrayList(fields.values)
    }

    fun headers(): List<CustomHeader> {
        return ArrayList(headers.values)
    }

    data class Builder(
        var typeRequest: TypeRequest? = null,
        var endPoint: String = "",
        var authToken: String? = null,
    ) {
        fun typeRequest(typeRequest: TypeRequest) = apply { this.typeRequest = typeRequest }
        fun endPoint(endPoint: String) = apply { this.endPoint = endPoint }
        fun authToken(authToken: String) = apply { this.authToken = authToken }
        fun build() = ServerRequest(typeRequest, endPoint, authToken)
    }

    companion object {
        fun create(endPoint: String = "", authToken: String = ""): ServerRequest {
            var typeRequest = TypeRequest.BASIC_REQUEST
            if (authToken.isNotBlank()) {
                typeRequest = TypeRequest.AUTH_REQUEST
            }
            return Builder()
                .typeRequest(typeRequest)
                .endPoint(endPoint)
                .authToken(authToken)
                .build()
        }
    }
}