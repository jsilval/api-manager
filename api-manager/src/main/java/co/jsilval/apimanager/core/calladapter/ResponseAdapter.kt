package co.jsilval.apimanager.core.calladapter

import retrofit2.CallAdapter
import retrofit2.Call
import java.lang.reflect.Type
import java.util.concurrent.Executor

class ResponseAdapter<R> internal constructor(
    private val responseType: Type,
    private val callbackExecutor: Executor?
) : CallAdapter<R, ResponseCall<R>> {

    override fun responseType(): Type {
        return responseType
    }

    override fun adapt(call: Call<R>): ResponseCall<R> {
        return ResponseCallAdapter(call, callbackExecutor)
    }
}