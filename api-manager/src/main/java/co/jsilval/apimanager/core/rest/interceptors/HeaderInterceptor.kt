package co.jsilval.apimanager.core.rest.interceptors

import okhttp3.Interceptor.Chain
import co.jsilval.apimanager.core.rest.headers.CustomHeader
import android.text.TextUtils
import android.util.Log
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Request.Builder
import okhttp3.Response
import java.io.IOException

class HeaderInterceptor : Interceptor {
    private var headers: List<CustomHeader>? = null

    constructor()
    constructor(headers: List<CustomHeader>) {
        this.headers = headers
    }

    fun setHeaders(headers: List<CustomHeader>): HeaderInterceptor {
        this.headers = headers
        return this
    }

    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        val original: Request = chain.request()
        val builder: Builder = original.newBuilder()
        headers?.let { headers ->
            for (header in headers) {
                builder.header(header.name, header.value)
            }
            Log.d("HeaderInterceptor", "intercept: " + TextUtils.join(", ", headers))
        }
        return chain.proceed(builder.build())
    }
}