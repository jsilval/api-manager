package co.jsilval.apimanager.core.rest.network

enum class HttpMethod {
    GET, POST, PUT, PATCH, DELETE
}