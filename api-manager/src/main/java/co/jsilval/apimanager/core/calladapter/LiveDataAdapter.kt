package co.jsilval.apimanager.core.calladapter

import retrofit2.CallAdapter
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import retrofit2.Call
import java.lang.reflect.Type

class LiveDataAdapter<R>(private val responseType: Type) :
    CallAdapter<R, LiveData<ApiResponse>> {
    override fun responseType(): Type {
        return responseType
    }

    override fun adapt(call: Call<R>): LiveData<ApiResponse> {
        return ResponseLiveDataCallAdapter(call)
    }
}