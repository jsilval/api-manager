package co.jsilval.apimanager.core.rest.network.mapper

import co.jsilval.apimanager.core.rest.field.Field
import java.util.HashMap

object FieldsMapper {
    @JvmStatic
    fun toMapFields(fields: List<Field>): Map<String, String> {
        val map = HashMap<String, String>()
        for (field in fields) {
            map.putAll(field.map)
        }
        return map
    }
}