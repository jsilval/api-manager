package co.jsilval.apimanager.core.rest

import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse

interface RequestCallback<T> {
    fun responseSuccess(response: ApiSuccessResponse<T>)
    fun responseEmpty(response: ApiEmptyResponse)
    fun responseError(response: ApiErrorResponse)
    fun otherError(t: Throwable)
    fun onFailure(t: Throwable)
}