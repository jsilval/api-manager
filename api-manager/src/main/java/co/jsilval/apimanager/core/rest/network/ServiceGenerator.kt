package co.jsilval.apimanager.core.rest.network

import android.text.TextUtils
import co.jsilval.apimanager.BuildConfig
import co.jsilval.apimanager.core.rest.headers.CustomHeader
import co.jsilval.apimanager.core.rest.interceptors.AuthInterceptor
import okhttp3.OkHttpClient
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.*

object ServiceGenerator {
    private val httpClient = OkHttpClient.Builder()

    fun <S> createService(serviceClass: Class<S>, headers: List<CustomHeader>): S {
        if (!httpClient.interceptors().contains(ApiManager.logging)) {
            httpClient.addInterceptor(ApiManager.logging)
        }
        if (headers.isNotEmpty()) {
            if (!httpClient.interceptors().contains(ApiManager.headerInterceptor)) {
                httpClient.addInterceptor(ApiManager.headerInterceptor.setHeaders(headers))
            } else {
                ApiManager.headerInterceptor.setHeaders(headers)
            }
        }
        if (ApiManager.useUnsafeOkHttpClient) {
            setUnsafeOkHttpClient()
        }
        httpClient.readTimeout(ApiManager.readTimeout, TimeUnit.MILLISECONDS)
            .connectTimeout(ApiManager.connectTimeout, TimeUnit.MILLISECONDS)
        ApiManager.builder.client(httpClient.build())
        ApiManager.retrofit = ApiManager.builder.build()
        return ApiManager.retrofit.create(serviceClass)
    }

    fun <S> createService(serviceClass: Class<S>): S {
        if (!httpClient.interceptors().contains(ApiManager.logging)) {
            httpClient.addInterceptor(ApiManager.logging)
        }
        if (ApiManager.useUnsafeOkHttpClient) {
            setUnsafeOkHttpClient()
        }
        httpClient.readTimeout(ApiManager.readTimeout, TimeUnit.MILLISECONDS)
            .connectTimeout(ApiManager.connectTimeout, TimeUnit.MILLISECONDS)
        ApiManager.builder.client(httpClient.build())
        ApiManager.retrofit = ApiManager.builder.build()
        return ApiManager.retrofit.create(serviceClass)
    }

    fun <S> createService(serviceClass: Class<S>, authToken: String?): S {
        if (ApiManager.useUnsafeOkHttpClient) {
            setUnsafeOkHttpClient()
        }
        if (!TextUtils.isEmpty(authToken)) {
            val interceptor = AuthInterceptor(authToken)
            if (!httpClient.interceptors().contains(interceptor)) {
                httpClient.addInterceptor(interceptor)
                if (BuildConfig.DEBUG) {
                    httpClient.addInterceptor(ApiManager.logging)
                }
                httpClient.readTimeout(ApiManager.readTimeout, TimeUnit.MILLISECONDS)
                    .connectTimeout(ApiManager.connectTimeout, TimeUnit.MILLISECONDS)
                ApiManager.builder.client(httpClient.build())
                ApiManager.retrofit = ApiManager.builder.build()
            }
        }
        return ApiManager.retrofit.create(serviceClass)
    }

    private fun setUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(
                object : X509TrustManager {
                    @Throws(CertificateException::class)
                    override fun checkClientTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    @Throws(CertificateException::class)
                    override fun checkServerTrusted(
                        chain: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    override fun getAcceptedIssuers(): Array<X509Certificate> {
                        return arrayOf()
                    }
                }
            )

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory
            httpClient.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            httpClient.hostnameVerifier { _: String?, _: SSLSession? -> true }
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }
}