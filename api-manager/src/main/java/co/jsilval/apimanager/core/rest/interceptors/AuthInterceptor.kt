package co.jsilval.apimanager.core.rest.interceptors

import android.util.Log
import co.jsilval.apimanager.core.rest.headers.Header
import okhttp3.Interceptor
import okhttp3.Interceptor.Chain
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class AuthInterceptor(private val authToken: String?) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Chain): Response {
        var request: Request = chain.request()

        // if NoAuth-flag header is missing, we can add authentication
        request =
            if (request.header("AppInternal-NoAuth") == null && authToken != null && authToken.isNotEmpty()) {
                request
                    .newBuilder()
                    .header(Header.AUTHORIZATION.key, authToken)
                    .build()
            } else {
                request
                    .newBuilder()
                    .removeHeader("AppInternal-NoAuth")
                    .build()
            }

        // if LogRequest-flag header is there, log the request body
        if (request.header("AppInternal-LogRequest") != null) {
            Log.d(TAG, "Logging request to URL: " + request.url.toString())
        }
        return chain.proceed(request)
    }

    companion object {
        private val TAG = AuthInterceptor::class.java.simpleName
    }
}