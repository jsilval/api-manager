package co.jsilval.apimanager.core.calladapter

import retrofit2.CallAdapter
import retrofit2.Retrofit
import co.jsilval.apimanager.core.calladapter.ResponseCall
import co.jsilval.apimanager.core.calladapter.ResponseAdapter
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class ResponseAdapterFactory : CallAdapter.Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (getRawType(returnType) != ResponseCall::class.java) {
            return null
        }
        check(returnType is ParameterizedType) { "ResponseCall must have generic type (e.g., ResponseCall<ResponseBody>)" }
        val responseType = getParameterUpperBound(0, returnType)
        val callbackExecutor = retrofit.callbackExecutor()
        return ResponseAdapter<Any>(responseType, callbackExecutor)
    }
}