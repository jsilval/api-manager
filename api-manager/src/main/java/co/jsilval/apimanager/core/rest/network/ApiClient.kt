package co.jsilval.apimanager.core.rest.network

import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.network.response.SynchronousResponse
import com.google.gson.reflect.TypeToken
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import co.jsilval.apimanager.feature.post.POSTClient
import co.jsilval.apimanager.feature.get.GETClient
import co.jsilval.apimanager.feature.put.PUTClient
import co.jsilval.apimanager.feature.patch.PATCHClient
import co.jsilval.apimanager.feature.delete.DELETEClient
import java.lang.RuntimeException

class ApiClient(val serverRequest: ServerRequest, val httpMethod: HttpMethod) {
    @PublishedApi
    internal var requestCallback: RequestCallback<*>? = null
    var lastSynchronousResponse: SynchronousResponse<*>? = null

    @PublishedApi
    internal inline fun <reified T> doPostLiveData(): LiveData<ApiResponse> {
        val client = POSTClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        return client.asLiveData(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doPost(requestCallback: RequestCallback<T>) {
        val client = POSTClient(requestCallback)
        client.setResponseType(object : TypeToken<T>() {})
        client.execute(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doPost(): T? {
        val client = POSTClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        val result = client.synchronousCall(serverRequest)
        lastSynchronousResponse = client.synchronousResponse
        return result
    }

    @PublishedApi
    internal inline fun <reified T> doGet(requestCallback: RequestCallback<T>) {
        val client = GETClient(requestCallback)
        client.setResponseType(object : TypeToken<T>() {})
        client.execute(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doGet(): T? {
        val client = GETClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        val result = client.synchronousCall(serverRequest)
        lastSynchronousResponse = client.synchronousResponse
        return result
    }

    @PublishedApi
    internal inline fun <reified T> doGetLiveData(): LiveData<ApiResponse> {
        val client = GETClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        return client.asLiveData(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doPut(requestCallback: RequestCallback<T>) {
        val client = PUTClient(requestCallback)
        client.setResponseType(object : TypeToken<T>() {})
        client.execute(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doPut(): T? {
        val client = PUTClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        val result = client.synchronousCall(serverRequest)
        lastSynchronousResponse = client.synchronousResponse
        return result
    }

    @PublishedApi
    internal inline fun <reified T> doPutLiveData(): LiveData<ApiResponse> {
        val client = PUTClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        return client.asLiveData(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doPatch(requestCallback: RequestCallback<T>) {
        val client = PATCHClient(requestCallback)
        client.setResponseType(object : TypeToken<T>() {})
        client.execute(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doPatch(): T? {
        val client = PATCHClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        val result = client.synchronousCall(serverRequest)
        lastSynchronousResponse = client.synchronousResponse
        return result
    }

    @PublishedApi
    internal inline fun <reified T> doPatchLiveData(): LiveData<ApiResponse> {
        val client = PATCHClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        return client.asLiveData(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doDelete(requestCallback: RequestCallback<T>) {
        val client = DELETEClient(requestCallback)
        client.setResponseType(object : TypeToken<T>() {})
        client.execute(serverRequest)
    }

    @PublishedApi
    internal inline fun <reified T> doDelete(): T? {
        val client = DELETEClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        val result = client.synchronousCall(serverRequest)
        lastSynchronousResponse = client.synchronousResponse
        return result
    }

    @PublishedApi
    internal inline fun <reified T> doDeleteLiveData(): LiveData<ApiResponse> {
        val client = DELETEClient<T>()
        client.setResponseType(object : TypeToken<T>() {})
        return client.asLiveData(serverRequest)
    }

    inline fun <reified T> getLiveData(): LiveData<ApiResponse> {
        return if (httpMethod === HttpMethod.GET) {
            doGetLiveData<T>()
        } else if (httpMethod === HttpMethod.POST) {
            doPostLiveData<T>()
        } else if (httpMethod === HttpMethod.PUT) {
            doPutLiveData<T>()
        } else if (httpMethod === HttpMethod.PATCH) {
            doPatchLiveData<T>()
        } else if (httpMethod === HttpMethod.DELETE) {
            doDeleteLiveData<T>()
        } else {
            throw RuntimeException("Invalid Http method!")
        }
    }

    inline fun <reified T> execute(): T? {
        return if (httpMethod === HttpMethod.GET) {
            doGet()
        } else if (httpMethod === HttpMethod.POST) {
            doPost()
        } else if (httpMethod === HttpMethod.PUT) {
            doPut()
        } else if (httpMethod === HttpMethod.PATCH) {
            doPatch()
        } else if (httpMethod === HttpMethod.DELETE) {
            doDelete()
        } else {
            throw RuntimeException("Invalid Http method!")
        }
    }

    inline fun <reified T> execute(
        requestCallback: RequestCallback<T>,
    ) {
        this.requestCallback = requestCallback
        if (httpMethod === HttpMethod.GET) {
            doGet(requestCallback)
        } else if (httpMethod === HttpMethod.POST) {
            doPost(requestCallback)
        } else if (httpMethod === HttpMethod.PUT) {
            doPut(requestCallback)
        } else if (httpMethod === HttpMethod.PATCH) {
            doPatch(requestCallback)
        } else if (httpMethod === HttpMethod.DELETE) {
            doDelete(requestCallback)
        } else {
            throw RuntimeException("Invalid Http method!")
        }
    }

    fun removeListener() {
        requestCallback = null
    }
}