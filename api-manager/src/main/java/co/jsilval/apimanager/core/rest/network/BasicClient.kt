package co.jsilval.apimanager.core.rest.network

import android.util.Log
import androidx.annotation.CallSuper
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.calladapter.ResponseCall
import co.jsilval.apimanager.core.calladapter.ResponseCallback
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.network.converter.ResponseConverter.convert
import co.jsilval.apimanager.core.rest.network.response.*
import co.jsilval.apimanager.core.rest.network.response.ApiResponseCreator.create
import co.jsilval.apimanager.core.rest.network.response.ApiResponseCreator.createSuccessResponse
import co.jsilval.apimanager.core.safeLet
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import java.io.IOException
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.reflect.KClass

abstract class BasicClient<T>(private val requestCallback: RequestCallback<T>?) {
    protected var serverRequest: ServerRequest? = null
    protected var endPoint = ""
    var synchronousResponse: SynchronousResponse<ResponseBody>? = null
    private var attempt = 3
    private var responseType: TypeToken<T>? = null

    constructor() : this(null)

    fun setResponseType(typeToken: TypeToken<T>) {
        responseType = typeToken
    }

    protected fun executeSynchronous(call: ResponseCall<ResponseBody>): T? {
        synchronousResponse = call.execute()
        safeLet(synchronousResponse,
            responseType,
            serverRequest) { synchronousResponse, responseType, serverRequest ->
            return convert(
                responseType,
                synchronousResponse.response!!.string(),
                serverRequest.contentType)
        }
        return null
    }

    protected fun asLiveData(call: ResponseCall<ResponseBody>): LiveData<ApiResponse> {
        return object : LiveData<ApiResponse>() {
            private val started = AtomicBoolean(false)
            override fun onActive() {
                super.onActive()
                if (started.compareAndSet(false, true)) {
                    call.enqueue(object : ResponseCallback<ResponseBody> {
                        override fun responseSuccess(response: ApiSuccessResponse<ResponseBody>) {
                            try {
                                val result = convert(
                                    responseType!!,
                                    response.body.string(),
                                    serverRequest!!.contentType)
                                postValue(createSuccessResponse(result))
                            } catch (e: IOException) {
                                e.printStackTrace()
                                postValue(create(e))
                            }
                        }

                        override fun responseEmpty(response: ApiEmptyResponse) {
                            postValue(ApiEmptyResponse())
                        }

                        override fun responseError(response: ApiErrorResponse) {
                            postValue(create(Throwable(response.errorMessage)))
                        }

                        override fun otherError(t: Throwable) {
                            postValue(create(t))
                        }

                        override fun conversionError(t: Throwable) {
                            postValue(create(t))
                        }
                    })
                }
            }
        }
    }

    protected fun execute(call: ResponseCall<ResponseBody>) {
        call.enqueue(object : ResponseCallback<ResponseBody> {
            override fun responseSuccess(response: ApiSuccessResponse<ResponseBody>) {
                try {
                    safeLet(requestCallback,
                        responseType,
                        serverRequest) { callback, responseType, serverRequest ->
                        val result = convert(
                            responseType,
                            response.body.string(),
                            serverRequest.contentType)
                        result?.let {
                            callback.responseSuccess(createSuccessResponse(it))
                        }
                    }
                } catch (e: Exception) {
                    if (e is RuntimeException) {
                        conversionError(e)
                    } else {
                        otherError(e)
                    }
                }
            }

            override fun responseEmpty(response: ApiEmptyResponse) {
                Log.d(javaClass.simpleName, "Empty response")
                requestCallback?.responseEmpty(response)
            }

            override fun responseError(response: ApiErrorResponse) {
                Log.d(javaClass.simpleName, "Response from server: " + response.errorMessage)
                if (attempt != 0) {
                    --attempt
                    Log.d(javaClass.simpleName, "responseError --> remaining attempts $attempt")
                    execute(call)
                } else {
                    requestCallback?.responseError(response)
                }
            }

            override fun otherError(t: Throwable) {
                Log.e(javaClass.simpleName, "Other kind of error")
                requestCallback?.otherError(t)
            }

            override fun conversionError(t: Throwable) {
                Log.e(javaClass.simpleName, "Conversion error")
                t.printStackTrace()
                requestCallback?.onFailure(t)
            }
        })
    }

    private fun setEndPoint(serverRequest: ServerRequest) {
        this.serverRequest = serverRequest
        endPoint = serverRequest.endPoint
        val pathData = serverRequest.pathData
        if (pathData.isEmpty()) {
            return
        }
        for ((key, value) in pathData) {
            endPoint = endPoint.replace("{$key}", value)
        }
    }

    @CallSuper
    protected open fun execute(serverRequest: ServerRequest) {
        setEndPoint(serverRequest)
    }

    @CallSuper
    protected open fun asLiveData(serverRequest: ServerRequest): LiveData<ApiResponse>? {
        setEndPoint(serverRequest)
        return null
    }

    @CallSuper
    protected open fun synchronousCall(serverRequest: ServerRequest): T? {
        setEndPoint(serverRequest)
        return null
    }
}