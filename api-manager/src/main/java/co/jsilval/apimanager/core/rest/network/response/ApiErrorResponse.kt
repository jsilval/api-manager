package co.jsilval.apimanager.core.rest.network.response

class ApiErrorResponse(var errorMessage: String?) : ApiResponse() {

    init {
        var errorMessage = errorMessage
        if (errorMessage == null) {
            errorMessage = "unknown error"
        }
        this.errorMessage = errorMessage
    }
}