package co.jsilval.apimanager.core.rest.network

import co.jsilval.apimanager.core.calladapter.LiveDataCallAdapterFactory
import co.jsilval.apimanager.core.calladapter.ResponseAdapterFactory
import co.jsilval.apimanager.core.rest.interceptors.HeaderInterceptor
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.simplexml.SimpleXmlConverterFactory

object ApiManager {
    var baseURL = "http://127.0.0.1"
        set(value) {
            field = value
            builder = builder.baseUrl(field)
            retrofit = builder.build()
        }

    val logging: HttpLoggingInterceptor
    val headerInterceptor: HeaderInterceptor
    var builder: Retrofit.Builder
        private set
    var retrofit: Retrofit
    var useUnsafeOkHttpClient = false
    var readTimeout = 60000L
    var connectTimeout = 10000L

    init {
        builder = Retrofit.Builder()
            .addCallAdapterFactory(ResponseAdapterFactory())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .baseUrl(baseURL)
        retrofit = builder.build()
        logging = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)
        headerInterceptor = HeaderInterceptor()
    }
}