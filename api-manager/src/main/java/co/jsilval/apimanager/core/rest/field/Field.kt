package co.jsilval.apimanager.core.rest.field

import java.util.HashMap

class Field private constructor(val name: String, private val value: String) {

    override fun toString(): String {
        return "$name=$value"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Field) return false
        return name.equals(other.name, ignoreCase = true)
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    val map: HashMap<String, String>
        get() {
            val map = HashMap<String, String>()
            map[name] = value
            return map
        }

    companion object {
        @JvmStatic
        fun create(name: String, value: String): Field {
            return Field(name, value)
        }
    }
}