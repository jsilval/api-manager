package co.jsilval.apimanager.core.calladapter

import co.jsilval.apimanager.core.rest.network.response.ApiResponseCreator.create
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import co.jsilval.apimanager.core.rest.network.response.ApiResponseCreator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.atomic.AtomicBoolean

class ResponseLiveDataCallAdapter<T>(private val call: Call<T>) : LiveData<ApiResponse>() {
    private val started = AtomicBoolean(false)
    override fun onActive() {
        super.onActive()
        if (started.compareAndSet(false, true)) {
            call.enqueue(object : Callback<T> {
                override fun onResponse(call: Call<T>, response: Response<T>) {
                    postValue(create(response))
                }

                override fun onFailure(call: Call<T>, t: Throwable) {
                    postValue(create(t))
                }
            })
        }
    }
}