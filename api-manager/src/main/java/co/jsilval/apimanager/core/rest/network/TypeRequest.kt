package co.jsilval.apimanager.core.rest.network

enum class TypeRequest {
    AUTH_REQUEST, BASIC_REQUEST
}