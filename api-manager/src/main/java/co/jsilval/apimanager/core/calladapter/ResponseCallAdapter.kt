package co.jsilval.apimanager.core.calladapter

import co.jsilval.apimanager.core.rest.network.response.ApiResponseCreator.create
import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.core.rest.network.response.SynchronousResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.lang.RuntimeException
import java.util.concurrent.Executor

class ResponseCallAdapter<T> internal constructor(
    private val call: Call<T>,
    private val callbackExecutor: Executor?
) : ResponseCall<T> {
    override fun cancel() {
        call.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    override fun enqueue(callback: ResponseCallback<T>) {
        call.clone().enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                val apiResponse = create(response)
                if (apiResponse is ApiSuccessResponse<*>) {
                    callbackExecutor?.execute { callback.responseSuccess((apiResponse as ApiSuccessResponse<T>)) }
                } else if (apiResponse is ApiEmptyResponse) {
                    callbackExecutor?.execute { callback.responseEmpty(apiResponse) }
                } else if (apiResponse is ApiErrorResponse) {
                    callbackExecutor?.execute { callback.responseError(apiResponse) }
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                if (t is RuntimeException) {
                    callbackExecutor?.execute { callback.conversionError(t) }
                } else {
                    callbackExecutor?.execute { callback.otherError(t) }
                }
            }
        })
    }

    override fun execute(): SynchronousResponse<T>? {
        try {
            val response = call.execute()
            var errorMessage: String? = null
            if (response.errorBody() != null) {
                errorMessage = response.errorBody()!!.string()
            }
            return SynchronousResponse(errorMessage, response.code(), response.body())
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }
}