package co.jsilval.apimanager.core.rest.network.response

import co.jsilval.apimanager.core.rest.network.converter.ResponseConverter.convert
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import java.io.IOException
import java.util.*

class ApiSuccessResponse<T>(var body: T) : ApiResponse() {

    fun <K> convert(responseType: TypeToken<K>): K? {
        if (body is ResponseBody) {
            try {
                val response = (body as ResponseBody).string()
                checkNotNull((body as ResponseBody).contentType())
                val contentType = (body as ResponseBody).contentType().toString()
                return convert(
                    responseType,
                    response,
                    contentType)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return null
    }
}