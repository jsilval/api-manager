package co.jsilval.apimanager.core.rest.network.response

import retrofit2.Response

object ApiResponseCreator {
    @JvmStatic
    fun create(error: Throwable): ApiErrorResponse {
        return ApiErrorResponse(error.message)
    }

    @JvmStatic
    fun create(errorMessage: String?): ApiErrorResponse {
        return ApiErrorResponse(errorMessage)
    }

    @JvmStatic
    fun <T> createSuccessResponse(response: T): ApiSuccessResponse<T> {
        return ApiSuccessResponse(response)
    }

    @JvmStatic
    fun <T> create(response: Response<T>?): ApiResponse {
        return if (response != null && response.isSuccessful) {
            val body = response.body()
            if (body == null || response.code() == 204) {
                ApiEmptyResponse()
            } else {
                ApiSuccessResponse(body)
            }
        } else {
            var msg = ""
            response?.errorBody()?.let { error ->
                msg = error.string()
            }

            val errorMsg: String = msg.ifEmpty {
                response?.message() ?: "Not error message, response is NULL"
            }
            ApiErrorResponse(errorMsg)
        }
    }
}