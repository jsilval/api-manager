package co.jsilval.apimanager.core.rest.headers

class CustomHeader private constructor(var name: String, var value: String) {
    fun setKey(header: Header) {
        name = header.key
    }

    override fun toString(): String {
        return "($name, $value)"
    }

    companion object {
        @JvmStatic
        fun create(key: String, value: String): CustomHeader {
            return CustomHeader(key, value)
        }
    }
}