package co.jsilval.apimanager.feature.post

import co.jsilval.apimanager.core.calladapter.ResponseCall
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import okhttp3.ResponseBody
import retrofit2.http.*

interface POSTService {
    @POST
    fun post(@Url url: String, @Body body: Any?): ResponseCall<ResponseBody>

    @POST
    @FormUrlEncoded
    fun post(
        @Url url: String,
        @FieldMap fields: Map<String, String>
    ): ResponseCall<ResponseBody>

    @POST
    fun postAsLiveData(@Url url: String, @Body body: Any?): LiveData<ApiResponse>

    @POST
    @FormUrlEncoded
    fun postAsLiveData(
        @Url url: String,
        @FieldMap fields: Map<String, String>
    ): LiveData<ApiResponse>
}