package co.jsilval.apimanager.feature.put

import co.jsilval.apimanager.core.calladapter.ResponseCall
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import okhttp3.ResponseBody
import retrofit2.http.*

interface PUTService {
    @PUT
    fun put(@Url url: String, @Body body: Any?): ResponseCall<ResponseBody>

    @PUT
    @FormUrlEncoded
    fun put(
        @Url url: String,
        @FieldMap fields: Map<String, String>
    ): ResponseCall<ResponseBody>

    @PUT
    fun putAsLiveData(@Url url: String, @Body body: Any): LiveData<ApiResponse>

    @PUT
    @FormUrlEncoded
    fun putAsLiveData(
        @Url url: String,
        @FieldMap fields: Map<String, String>
    ): LiveData<ApiResponse>
}