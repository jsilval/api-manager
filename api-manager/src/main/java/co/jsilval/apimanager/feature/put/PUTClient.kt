package co.jsilval.apimanager.feature.put

import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.network.BasicClient
import co.jsilval.apimanager.core.rest.network.ServiceGenerator.createService
import co.jsilval.apimanager.core.rest.network.mapper.FieldsMapper.toMapFields
import co.jsilval.apimanager.core.rest.network.response.ApiResponse

class PUTClient<T>(requestCallback: RequestCallback<T>?) : BasicClient<T>(requestCallback) {
    constructor() : this(null)

    public override fun synchronousCall(serverRequest: ServerRequest): T? {
        super.synchronousCall(serverRequest)
        this.serverRequest = serverRequest
        val putService = createService(PUTService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            putService.put(endPoint, serverRequest.body)
        } else {
            putService.put(endPoint, toMapFields(serverRequest.fieldList()))
        }
        return executeSynchronous(call)
    }

    public override fun execute(serverRequest: ServerRequest) {
        super.execute(serverRequest)
        this.serverRequest = serverRequest
        val putService = createService(PUTService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            putService.put(endPoint, serverRequest.body)
        } else {
            putService.put(endPoint, toMapFields(serverRequest.fieldList()))
        }
        execute(call)
    }

    public override fun asLiveData(serverRequest: ServerRequest): LiveData<ApiResponse> {
        super.asLiveData(serverRequest)
        this.serverRequest = serverRequest
        val putService = createService(PUTService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            putService.put(endPoint, serverRequest.body)
        } else {
            putService.put(endPoint, toMapFields(serverRequest.fieldList()))
        }
        return asLiveData(call)
    }
}