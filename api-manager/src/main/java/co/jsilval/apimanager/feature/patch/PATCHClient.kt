package co.jsilval.apimanager.feature.patch

import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.calladapter.ResponseCall
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.network.BasicClient
import co.jsilval.apimanager.core.rest.network.ServiceGenerator.createService
import co.jsilval.apimanager.core.rest.network.mapper.FieldsMapper.toMapFields
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import okhttp3.ResponseBody

class PATCHClient<T>(requestCallback: RequestCallback<T>?) : BasicClient<T>(requestCallback) {
    constructor(): this(null)

    public override fun synchronousCall(serverRequest: ServerRequest): T? {
        super.synchronousCall(serverRequest)
        this.serverRequest = serverRequest
        val patchService = createService(
            PATCHService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            patchService.patch(endPoint, serverRequest.body)
        } else {
            patchService.patch(endPoint, toMapFields(serverRequest.fieldList()))
        }
        return executeSynchronous(call)
    }

    public override fun execute(serverRequest: ServerRequest) {
        super.execute(serverRequest)
        this.serverRequest = serverRequest
        val patchService = createService(
            PATCHService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            patchService.patch(endPoint, serverRequest.body)
        } else {
            patchService.patch(endPoint, toMapFields(serverRequest.fieldList()))
        }
        execute(call)
    }

    public override fun asLiveData(serverRequest: ServerRequest): LiveData<ApiResponse> {
        super.asLiveData(serverRequest)
        this.serverRequest = serverRequest
        val patchService = createService(
            PATCHService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            patchService.patch(endPoint, serverRequest.body)
        } else {
            patchService.patch(endPoint, toMapFields(serverRequest.fieldList()))
        }
        return asLiveData(call)
    }
}