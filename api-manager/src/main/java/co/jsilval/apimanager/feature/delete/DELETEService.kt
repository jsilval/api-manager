package co.jsilval.apimanager.feature.delete

import retrofit2.http.DELETE
import retrofit2.http.Url
import co.jsilval.apimanager.core.calladapter.ResponseCall
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import okhttp3.ResponseBody
import retrofit2.http.QueryMap

interface DELETEService {
    @DELETE
    fun delete(@Url url: String): ResponseCall<ResponseBody>

    @DELETE
    fun delete(
        @Url url: String,
        @QueryMap fields: Map<String, String>
    ): ResponseCall<ResponseBody>

    @DELETE
    fun deleteAsLiveData(@Url url: String): LiveData<ApiResponse>

    @DELETE
    fun deleteAsLiveData(
        @Url url: String,
        @QueryMap fields: Map<String, String>
    ): LiveData<ApiResponse>
}