package co.jsilval.apimanager.feature.get

import retrofit2.http.GET
import retrofit2.http.Url
import co.jsilval.apimanager.core.calladapter.ResponseCall
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import okhttp3.ResponseBody
import retrofit2.http.QueryMap

interface GETService {
    @GET
    operator fun get(@Url url: String): ResponseCall<ResponseBody>

    @GET
    operator fun get(
        @Url url: String,
        @QueryMap fields: Map<String, String>
    ): ResponseCall<ResponseBody>

    @GET
    fun getAsLiveData(@Url url: String): LiveData<ApiResponse>

    @GET
    fun getAsLiveData(
        @Url url: String,
        @QueryMap fields: Map<String, String>
    ): LiveData<ApiResponse>
}