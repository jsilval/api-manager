package co.jsilval.apimanager.feature.patch

import co.jsilval.apimanager.core.calladapter.ResponseCall
import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.network.response.ApiResponse
import okhttp3.ResponseBody
import retrofit2.http.*

interface PATCHService {
    @PATCH
    fun patch(@Url url: String, @Body body: Any?): ResponseCall<ResponseBody>

    @PATCH
    fun patchAsLiveData(@Url url: String, @Body body: Any?): LiveData<ApiResponse>

    @PATCH
    @FormUrlEncoded
    fun patch(
        @Url url: String,
        @FieldMap fields: Map<String, String>
    ): ResponseCall<ResponseBody>

    @PATCH
    @FormUrlEncoded
    fun patchAsLiveData(
        @Url url: String,
        @FieldMap fields: Map<String, String>
    ): LiveData<ApiResponse>
}