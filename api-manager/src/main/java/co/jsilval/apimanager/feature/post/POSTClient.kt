package co.jsilval.apimanager.feature.post

import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.network.BasicClient
import co.jsilval.apimanager.core.rest.network.ServiceGenerator.createService
import co.jsilval.apimanager.core.rest.network.mapper.FieldsMapper.toMapFields
import co.jsilval.apimanager.core.rest.network.response.ApiResponse

class POSTClient<T>(requestCallback: RequestCallback<T>?) : BasicClient<T>(requestCallback) {
    constructor() : this(null)

    public override fun synchronousCall(serverRequest: ServerRequest): T? {
        super.synchronousCall(serverRequest)
        val postService = createService(POSTService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            postService.post(endPoint, serverRequest.body)
        } else {
            postService.post(endPoint, toMapFields(serverRequest.fieldList()))
        }
        return executeSynchronous(call)
    }

    public override fun execute(serverRequest: ServerRequest) {
        super.execute(serverRequest)
        val postService = createService(POSTService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            postService.post(endPoint, serverRequest.body)
        } else {
            postService.post(endPoint, toMapFields(serverRequest.fieldList()))
        }
        execute(call)
    }

    public override fun asLiveData(serverRequest: ServerRequest): LiveData<ApiResponse> {
        super.asLiveData(serverRequest)
        val postService = createService(POSTService::class.java, serverRequest.headers())
        val call = if (serverRequest.body != null) {
            postService.post(endPoint, serverRequest.body)
        } else {
            postService.post(endPoint, toMapFields(serverRequest.fieldList()))
        }
        return asLiveData(call)
    }
}