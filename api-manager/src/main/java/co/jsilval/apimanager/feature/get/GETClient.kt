package co.jsilval.apimanager.feature.get

import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.network.BasicClient
import co.jsilval.apimanager.core.rest.network.ServiceGenerator.createService
import co.jsilval.apimanager.core.rest.network.mapper.FieldsMapper.toMapFields
import co.jsilval.apimanager.core.rest.network.response.ApiResponse

class GETClient<T>(requestCallback: RequestCallback<T>?) : BasicClient<T>(requestCallback) {
    constructor() : this(null)

    public override fun execute(serverRequest: ServerRequest) {
        super.execute(serverRequest)
        val getService = createService(GETService::class.java, serverRequest.headers())
        val call = if (serverRequest.fieldList().isNotEmpty()) {
            getService.get(endPoint, toMapFields(serverRequest.fieldList()))
        } else {
            getService.get(endPoint)
        }
        execute(call)
    }

    public override fun synchronousCall(serverRequest: ServerRequest): T? {
        super.synchronousCall(serverRequest)
        val getService = createService(GETService::class.java,
            serverRequest.headers())
        val call = if (serverRequest.fieldList().isNotEmpty()) {
            getService[endPoint, toMapFields(serverRequest.fieldList())]
        } else {
            getService[endPoint]
        }
        return executeSynchronous(call)
    }

    public override fun asLiveData(serverRequest: ServerRequest): LiveData<ApiResponse> {
        super.asLiveData(serverRequest)
        val getService = createService(GETService::class.java,
            serverRequest.headers())
        val call = if (serverRequest.fieldList().isNotEmpty()) {
            getService[endPoint, toMapFields(serverRequest.fieldList())]
        } else {
            getService[endPoint]
        }
        return asLiveData(call)
    }
}