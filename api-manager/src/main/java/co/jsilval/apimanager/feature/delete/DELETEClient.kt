package co.jsilval.apimanager.feature.delete

import androidx.lifecycle.LiveData
import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.network.BasicClient
import co.jsilval.apimanager.core.rest.network.ServiceGenerator
import co.jsilval.apimanager.core.rest.network.mapper.FieldsMapper.toMapFields
import co.jsilval.apimanager.core.rest.network.response.ApiResponse

class DELETEClient<T>(requestCallback: RequestCallback<T>?) : BasicClient<T>(requestCallback) {
    constructor(): this(null)

    public override fun synchronousCall(serverRequest: ServerRequest): T? {
        super.synchronousCall(serverRequest)
        val deleteService = ServiceGenerator.createService(
            DELETEService::class.java,
            serverRequest.headers())
        val call = if (serverRequest.fieldList().isNotEmpty()) {
            deleteService.delete(endPoint, toMapFields(serverRequest.fieldList()))
        } else {
            deleteService.delete(endPoint)
        }
        return executeSynchronous(call)
    }

    public override fun asLiveData(serverRequest: ServerRequest): LiveData<ApiResponse> {
        super.asLiveData(serverRequest)
        val deleteService = ServiceGenerator.createService(
            DELETEService::class.java,
            serverRequest.headers())
        val call = if (serverRequest.fieldList().isNotEmpty()) {
            deleteService.delete(endPoint, toMapFields(serverRequest.fieldList()))
        } else {
            deleteService.delete(endPoint)
        }
        return asLiveData(call)
    }

    public override fun execute(serverRequest: ServerRequest) {
        super.execute(serverRequest)
        val deleteService = ServiceGenerator.createService(
            DELETEService::class.java,
            serverRequest.headers())
        val call = if (serverRequest.fieldList().isNotEmpty()) {
            deleteService.delete(endPoint, toMapFields(serverRequest.fieldList()))
        } else {
            deleteService.delete(endPoint)
        }
        execute(call)
    }
}