package co.jsilval.apimanager.example.entities

import com.google.gson.annotations.SerializedName

class UserPost {
    @SerializedName("userName")
    var userName: String? = null

    @SerializedName("id")
    var id = 0

    @SerializedName("password")
    var password: String? = null

    override fun toString(): String {
        return "UserPost{" +
                "userName = '" + userName + '\'' +
                ",id = '" + id + '\'' +
                ",password = '" + password + '\'' +
                "}"
    }
}