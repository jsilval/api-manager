package co.jsilval.apimanager.example

import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.example.domain.ExampleInteractor
import co.jsilval.apimanager.example.entities.UserPost
import co.jsilval.apimanager.example.entities.UserPut
import co.jsilval.apimanager.example.entities.UserResponse
import co.jsilval.apimanager.example.ui.ExampleContract
import co.jsilval.apimanager.example.ui.ExampleContract.Presenter

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File ExamplePresenter.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class ExamplePresenter(
    private var exampleView: ExampleContract.View?,
    private val getUser: ExampleInteractor.GetUser,
    postUser: ExampleInteractor.PostUser,
    putUser: ExampleInteractor.PutUser,
    deleteUser: ExampleInteractor.DeleteUser
) : Presenter, ExampleInteractor.GetUser.GetUserCallback,
    ExampleInteractor.PostUser.PostUserCallback, ExampleInteractor.PutUser.PutUserCallback,
    ExampleInteractor.DeleteUser.DeleteUserCallback {

    private val deleteUser: ExampleInteractor.DeleteUser
    private val postUser: ExampleInteractor.PostUser
    private val putUser: ExampleInteractor.PutUser

    override fun onDestroy() {
        exampleView = null
    }

    override val user: Unit
        get() {
            getUser.executeUseCase()
        }

    override fun postUser() {
        postUser.executeUseCase()
    }

    override fun putUSer() {
        putUser.executeUseCase()
    }

    override fun deleteUSer() {
        deleteUser.executeUseCase()
    }

    override fun onGetUserSuccess(response: UserResponse?) {
        if (exampleView == null) {
            return
        }
        exampleView!!.showResponse(response.toString())
    }

    override fun onGetUserError(response: ApiErrorResponse?) {
        if (exampleView == null) {
            return
        }
    }

    override fun onPostUserSuccess(response: UserPost?) {
        if (exampleView == null) {
            return
        }
        exampleView!!.showResponse(response.toString())
    }

    override fun onPostUserError(response: ApiErrorResponse?) {
        if (exampleView == null) {
            return
        }
    }

    override fun onPutUserSuccess(response: UserPut?) {
        if (exampleView == null) {
            return
        }
        exampleView!!.showResponse(response.toString())
    }

    override fun onPutUserError(response: ApiErrorResponse?) {
        if (exampleView == null) {
            return
        }
    }

    override fun onDeleteUserSuccess() {
        if (exampleView == null) {
            return
        }
        exampleView!!.showResponse("Usuario borrado")
    }

    override fun onDeleteUserError(response: ApiErrorResponse?) {}

    init {
        getUser.setCallback(this)
        this.postUser = postUser
        this.postUser.setCallback(this)
        this.putUser = putUser
        this.putUser.setCallback(this)
        this.deleteUser = deleteUser
        this.deleteUser.setCallback(this)
    }
}