package co.jsilval.apimanager.example.ui

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File ExampleContract.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
interface ExampleContract {
    interface View {
        fun showResponse(response: String)
    }

    interface Presenter {
        fun onDestroy()
        val user: Unit
        fun postUser()
        fun putUSer()
        fun deleteUSer()
    }
}