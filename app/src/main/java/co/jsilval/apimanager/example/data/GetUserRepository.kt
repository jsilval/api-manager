package co.jsilval.apimanager.example.data

import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.headers.Header
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.HttpMethod
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse
import co.jsilval.apimanager.example.entities.UserResponse

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File GetUserRepository.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class GetUserRepository : ExampleDataSource.GetUser,
    RequestCallback<List<UserResponse>> {
    private var callback: ExampleDataSource.GetUser.GetUserCallback? = null
    override fun getUser(callback: ExampleDataSource.GetUser.GetUserCallback) {
        this.callback = callback
        val serverRequest = ServerRequest.create("/api/{version}/Users/")
            .header(Header.CONTENT_TYPE, "application/json")
            .header(Header.ACCEPT_LANGUAGE, "es-CO")
        serverRequest.path("version", "v1")
        val client = ApiClient(serverRequest, HttpMethod.GET)
        client.execute(this)

//      ********************** LIVEDATA USE
//        val liveData = client.getLiveData<List<UserResponse>>()
//        liveData.observe(context as AppCompatActivity) { apiResponse ->
//            if (apiResponse is ApiSuccessResponse<*>) {
//                val list = (apiResponse as ApiSuccessResponse<List<UserResponse>>).body
//                Log.d("TAG", "onChanged: list size -> " + list.size)
//            }
//        }

//      *********************** SYNCHRONOUS USE
//        Thread {
//            val responseList = client.execute<List<UserResponse>>()
//            Log.d("TAG", "run: " + responseList?.size);
//        }.start()
    }

    override fun responseSuccess(response: ApiSuccessResponse<List<UserResponse>>) {
        callback?.onGetUserSuccess(response.body[0])
    }

    override fun responseEmpty(response: ApiEmptyResponse) {}
    override fun responseError(response: ApiErrorResponse) {
        callback?.onGetUserError(response)
    }

    override fun otherError(t: Throwable) {
        t.printStackTrace()
    }

    override fun onFailure(t: Throwable) {}
}