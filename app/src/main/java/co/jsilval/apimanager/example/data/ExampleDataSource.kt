package co.jsilval.apimanager.example.data

import co.jsilval.apimanager.example.entities.UserResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.example.entities.UserPost
import co.jsilval.apimanager.example.entities.UserPut

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File ExampleDataSource.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
interface ExampleDataSource {
    interface GetUser {
        fun getUser(callback: GetUserCallback)
        interface GetUserCallback {
            fun onGetUserSuccess(response: UserResponse?)
            fun onGetUserError(response: ApiErrorResponse?)
        }
    }

    interface PostUser {
        fun postUser(callback: PostUserCallback)
        interface PostUserCallback {
            fun onPostUserSuccess(response: UserPost?)
            fun onPostUserError(response: ApiErrorResponse?)
        }
    }

    interface PutUser {
        fun putUser(callback: PutUserCallback)
        interface PutUserCallback {
            fun onPutUserSuccess(response: UserPut?)
            fun onPutUserError(response: ApiErrorResponse?)
        }
    }

    interface DeleteUser {
        fun deleteUser(callback: DeleteUserCallback)
        interface DeleteUserCallback {
            fun onDeleteUserSuccess()
            fun onDeleteUserError(response: ApiErrorResponse?)
        }
    }
}