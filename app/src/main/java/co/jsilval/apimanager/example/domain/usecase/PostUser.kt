package co.jsilval.apimanager.example.domain.usecase

import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.example.data.ExampleDataSource
import co.jsilval.apimanager.example.domain.ExampleInteractor
import co.jsilval.apimanager.example.entities.UserPost

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File PostUser.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class PostUser(private val exampleDataSource: ExampleDataSource.PostUser) :
    ExampleInteractor.PostUser {
    private var callback: ExampleInteractor.PostUser.PostUserCallback? = null
    override fun setCallback(callback: ExampleInteractor.PostUser.PostUserCallback?) {
        this.callback = callback
    }

    override fun executeUseCase() {
        exampleDataSource.postUser(object : ExampleDataSource.PostUser.PostUserCallback {
            override fun onPostUserSuccess(response: UserPost?) {
                callback?.onPostUserSuccess(response)
            }

            override fun onPostUserError(response: ApiErrorResponse?) {
                callback?.onPostUserError(response)
            }
        })
    }
}