package co.jsilval.apimanager.example.entities

import com.google.gson.annotations.SerializedName

/*{
    "ID": 3,
    "UserName": "User 3",
    "Password": "Password3"
}*/
class UserResponse {
    @SerializedName("userName")
    var userName: String? = null

    @SerializedName("id")
    var iD = 0

    @SerializedName("password")
    var password: String? = null
    override fun toString(): String {
        return "UserResponse{" +
                "userName = '" + userName + '\'' +
                ",id = '" + iD + '\'' +
                ",password = '" + password + '\'' +
                "}"
    }
}