package co.jsilval.apimanager.example.domain.usecase

import co.jsilval.apimanager.example.entities.UserResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.example.data.ExampleDataSource
import co.jsilval.apimanager.example.domain.ExampleInteractor

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File GetUser.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class GetUser(private val exampleDataSource: ExampleDataSource.GetUser) :
    ExampleInteractor.GetUser {
    private var callback: ExampleInteractor.GetUser.GetUserCallback? = null
    override fun setCallback(callback: ExampleInteractor.GetUser.GetUserCallback?) {
        this.callback = callback
    }

    override fun executeUseCase() {
        exampleDataSource.getUser(object : ExampleDataSource.GetUser.GetUserCallback {
            override fun onGetUserSuccess(response: UserResponse?) {
                callback?.onGetUserSuccess(response)
            }

            override fun onGetUserError(response: ApiErrorResponse?) {
                callback?.onGetUserError(response)
            }
        })
    }
}