package co.jsilval.apimanager.example.data

import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.headers.Header
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.HttpMethod
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File PostUserRepository.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class DeleteUserRepository : ExampleDataSource.DeleteUser,
    RequestCallback<Any> {
    private var callback: ExampleDataSource.DeleteUser.DeleteUserCallback? = null
    override fun deleteUser(callback: ExampleDataSource.DeleteUser.DeleteUserCallback) {
        this.callback = callback
        val serverRequest = ServerRequest.create("/api/v1/Users/2")
            .header(Header.CONTENT_TYPE, "application/json")
            .field("songs", "sizayas")
        val client = ApiClient(serverRequest, HttpMethod.DELETE)
        client.execute(this)
    }

    override fun responseSuccess(response: ApiSuccessResponse<Any>) {
        callback?.onDeleteUserSuccess()
    }

    override fun responseEmpty(response: ApiEmptyResponse) {}

    override fun responseError(response: ApiErrorResponse) {
        callback?.onDeleteUserError(response)
    }

    override fun otherError(t: Throwable) {
        t.printStackTrace()
    }

    override fun onFailure(t: Throwable) {}
}