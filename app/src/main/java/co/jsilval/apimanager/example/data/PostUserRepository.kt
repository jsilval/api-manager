package co.jsilval.apimanager.example.data

import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.headers.Header
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.HttpMethod
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse
import co.jsilval.apimanager.example.entities.UserPost

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File PostUserRepository.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class PostUserRepository : ExampleDataSource.PostUser,
    RequestCallback<UserPost> {
    private var callback: ExampleDataSource.PostUser.PostUserCallback? = null
    override fun postUser(callback: ExampleDataSource.PostUser.PostUserCallback) {
        this.callback = callback
        val user = UserPost()
        user.id = 1
        user.password = "12345"
        user.userName = "John"
        val serverRequest = ServerRequest.create("/api/{version}/Users")
            .header(Header.CONTENT_TYPE, "application/json")
            .body(user)
        serverRequest.path("version", "v1").field("pernio", "carranza")
        val client = ApiClient(serverRequest, HttpMethod.POST)
        client.execute(this)
    }

    override fun responseSuccess(response: ApiSuccessResponse<UserPost>) {
        callback?.onPostUserSuccess(response.body)
    }

    override fun responseEmpty(response: ApiEmptyResponse) {}
    override fun responseError(response: ApiErrorResponse) {
        callback?.onPostUserError(response)
    }

    override fun otherError(t: Throwable) {
        t.printStackTrace()
    }

    /*
     * Other kind of error, for example network
     * */
    override fun onFailure(t: Throwable) {}
}