package co.jsilval.apimanager.example.data

import co.jsilval.apimanager.core.rest.RequestCallback
import co.jsilval.apimanager.core.rest.ServerRequest
import co.jsilval.apimanager.core.rest.headers.Header
import co.jsilval.apimanager.core.rest.network.ApiClient
import co.jsilval.apimanager.core.rest.network.HttpMethod
import co.jsilval.apimanager.core.rest.network.response.ApiEmptyResponse
import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.core.rest.network.response.ApiSuccessResponse
import co.jsilval.apimanager.example.entities.UserPost
import co.jsilval.apimanager.example.entities.UserPut

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File PostUserRepository.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class PutUserRepository : ExampleDataSource.PutUser,
    RequestCallback<UserPut> {
    private var callback: ExampleDataSource.PutUser.PutUserCallback? = null
    override fun putUser(callback: ExampleDataSource.PutUser.PutUserCallback) {
        this.callback = callback
        val user = UserPost()
        user.id = 1
        user.password = "12345"
        user.userName = "John"
        val serverRequest = ServerRequest.create("/api/v1/Users/2")
            .header(Header.CONTENT_TYPE, "application/json")
            .body(user)
        val client = ApiClient(serverRequest, HttpMethod.PUT)
        client.execute(this)
    }

    override fun responseSuccess(response: ApiSuccessResponse<UserPut>) {
        callback?.onPutUserSuccess(response.body)
    }

    override fun responseEmpty(response: ApiEmptyResponse) {}
    override fun responseError(response: ApiErrorResponse) {
        callback?.onPutUserError(response)
    }

    override fun otherError(t: Throwable) {
        t.printStackTrace()
    }

    override fun onFailure(t: Throwable) {}
}