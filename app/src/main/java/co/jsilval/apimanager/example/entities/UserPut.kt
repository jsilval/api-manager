package co.jsilval.apimanager.example.entities

import com.google.gson.annotations.SerializedName

class UserPut {
    @SerializedName("userName")
    var userName: String? = null

    @SerializedName("id")
    var iD = 0

    @SerializedName("password")
    var password: String? = null
    override fun toString(): String {
        return "UserPut{" +
                "userName = '" + userName + '\'' +
                ",id = '" + iD + '\'' +
                ",password = '" + password + '\'' +
                "}"
    }
}