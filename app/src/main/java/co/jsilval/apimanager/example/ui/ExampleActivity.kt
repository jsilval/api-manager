package co.jsilval.apimanager.example.ui

import androidx.appcompat.app.AppCompatActivity
import co.jsilval.apimanager.example.ui.ExampleContract.Presenter
import android.widget.TextView
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.Toolbar
import co.jsilval.apimanager.example.R
import co.jsilval.apimanager.core.rest.network.ApiManager
import co.jsilval.apimanager.example.ExamplePresenter
import co.jsilval.apimanager.example.data.GetUserRepository
import co.jsilval.apimanager.example.data.PostUserRepository
import co.jsilval.apimanager.example.data.PutUserRepository
import co.jsilval.apimanager.example.data.DeleteUserRepository
import co.jsilval.apimanager.example.domain.usecase.DeleteUser
import co.jsilval.apimanager.example.domain.usecase.GetUser
import co.jsilval.apimanager.example.domain.usecase.PostUser
import co.jsilval.apimanager.example.domain.usecase.PutUser

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File ExampleActivity.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class ExampleActivity : AppCompatActivity(), ExampleContract.View, View.OnClickListener {
    private var examplePresenter: Presenter? = null
    private var tvResponse: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_example)
        bindViews()
        ApiManager.baseURL = "https://fakerestapi.azurewebsites.net/"

        examplePresenter = ExamplePresenter(this,
            GetUser(GetUserRepository()),
            PostUser(PostUserRepository()),
            PutUser(PutUserRepository()),
            DeleteUser(DeleteUserRepository()))
    }

    private fun bindViews() {
        val btnPost = findViewById<View>(R.id.button)
        val btnGet = findViewById<View>(R.id.button2)
        val btnPut = findViewById<View>(R.id.button3)
        val btnDelete = findViewById<View>(R.id.button4)
        tvResponse = findViewById(R.id.tvResponse)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        btnGet.setOnClickListener(this)
        btnPost.setOnClickListener(this)
        btnPut.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
    }

    public override fun onDestroy() {
        super.onDestroy()
        examplePresenter!!.onDestroy()
    }

    override fun onClick(v: View) {
        tvResponse!!.text = "..."
        when (v.id) {
            R.id.button -> examplePresenter!!.postUser()
            R.id.button2 -> examplePresenter!!.user
            R.id.button3 -> examplePresenter!!.putUSer()
            R.id.button4 -> examplePresenter!!.deleteUSer()
            else -> {}
        }
    }

    override fun showResponse(response: String) {
        tvResponse!!.text = response
    }
}