package co.jsilval.apimanager.example.domain.usecase

import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.example.data.ExampleDataSource
import co.jsilval.apimanager.example.domain.ExampleInteractor
import co.jsilval.apimanager.example.entities.UserPut

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File PutUser.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class PutUser(private val exampleDataSource: ExampleDataSource.PutUser) :
    ExampleInteractor.PutUser {
    private var callback: ExampleInteractor.PutUser.PutUserCallback? = null
    override fun setCallback(callback: ExampleInteractor.PutUser.PutUserCallback?) {
        this.callback = callback
    }

    override fun executeUseCase() {
        exampleDataSource.putUser(object : ExampleDataSource.PutUser.PutUserCallback {
            override fun onPutUserSuccess(response: UserPut?) {
                callback?.onPutUserSuccess(response)
            }

            override fun onPutUserError(response: ApiErrorResponse?) {
                callback?.onPutUserError(response)
            }
        })
    }
}