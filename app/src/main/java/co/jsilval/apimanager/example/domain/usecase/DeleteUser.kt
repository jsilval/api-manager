package co.jsilval.apimanager.example.domain.usecase

import co.jsilval.apimanager.core.rest.network.response.ApiErrorResponse
import co.jsilval.apimanager.example.data.ExampleDataSource
import co.jsilval.apimanager.example.domain.ExampleInteractor

/**
 * Copyright 2018 ME, Inc. All rights reserved.
 * File GetUser.class, created at 25/06/19 09:56 AM for Networklib project.
 * *****
 * Class description.
 *
 * @author Jose Silva
 * @version 1.0, 25/06/19 09:56 AM
 * @since 1.0
 */
class DeleteUser(private val exampleDataSource: ExampleDataSource.DeleteUser) :
    ExampleInteractor.DeleteUser {
    private var callback: ExampleInteractor.DeleteUser.DeleteUserCallback? = null
    override fun setCallback(callback: ExampleInteractor.DeleteUser.DeleteUserCallback?) {
        this.callback = callback
    }

    override fun executeUseCase() {
        exampleDataSource.deleteUser(object : ExampleDataSource.DeleteUser.DeleteUserCallback {
            override fun onDeleteUserSuccess() {
                callback?.onDeleteUserSuccess()
            }

            override fun onDeleteUserError(response: ApiErrorResponse?) {
                callback?.onDeleteUserError(response)
            }
        })
    }
}