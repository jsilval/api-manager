package co.jsilval.apimanager.example.entities

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "User")
class UserResponseXML {
    @Element(name = "UserName")
    var userName: String? = null

    @Element(name = "ID")
    var iD = 0

    @Element(name = "Password")
    var password: String? = null
    override fun toString(): String {
        return "UserResponse{" +
                "userName = '" + userName + '\'' +
                ",iD = '" + iD + '\'' +
                ",password = '" + password + '\'' +
                "}"
    }
}